const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req,res) => {
    const messages = db.getMessages();
    return res.send(messages);
});

router.post('/', async (req, res, next) => {
    try {
        const message = {
            author: req.body.author,
            message: req.body.message,
        };

        if (!message.author || !message.message) {
            return res.status(400).send({error: 'Author and message must be present in the request'});
        }

        await db.addMessage(message);

        return res.send({message: 'Created new message'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
