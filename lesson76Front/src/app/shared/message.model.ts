export class Message {
  constructor(
    public id: string,
    public message: number,
    public author: string,
    public datetime: string,
  ) {
  }
}

export interface MessageData {
  message: string;
  author: string;
}
