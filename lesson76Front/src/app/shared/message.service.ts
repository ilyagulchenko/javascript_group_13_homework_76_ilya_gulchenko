import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message, MessageData } from './message.model';
import { environment } from '../../environments/environment';
import { map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messagesFetch = new Subject<Message[]>();

  constructor(private http: HttpClient) {}

  private messages: Message[] = [];

  getMessages() {
    return this.http.get<Message[]>(environment.apiUrl + '/messages').pipe(
      map(response => {
        return response.map(messageData => {
          return new Message(
            messageData.id,
            messageData.message,
            messageData.author,
            new Date(messageData.datetime).toLocaleString()
          );
        });
      })
    ).subscribe(messages => {
      this.messages = messages;
      this.messagesFetch.next(this.messages.slice(-30));
    })
  }

  sendMessage(messageData: MessageData) {
    return this.http.post(environment.apiUrl + '/messages', messageData);
  }
}
