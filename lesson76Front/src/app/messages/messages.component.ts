import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../shared/message.model';
import { MessageService } from '../shared/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  messagesChangeSubscription!: Subscription;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.messagesChangeSubscription = this.messageService.messagesFetch.subscribe((messages: Message[]) => {
      this.messages = messages;
    });
    this.messageService.getMessages();
  }

  ngOnDestroy() {
    this.messagesChangeSubscription.unsubscribe();
  }

}
