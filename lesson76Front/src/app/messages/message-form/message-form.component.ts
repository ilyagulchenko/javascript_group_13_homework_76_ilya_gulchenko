import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from '../../shared/message.service';
import { MessageData } from '../../shared/message.model';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.sass']
})
export class MessageFormComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(
    private messageService: MessageService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const messageData: MessageData = this.form.value;
    this.messageService.sendMessage(messageData).subscribe(() => {
      this.messageService.getMessages();
    });
  }

}
